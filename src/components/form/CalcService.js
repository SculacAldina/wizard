import React, { Fragment, useContext } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import Typography from '@mui/material/Typography';
import { makeStyles } from '@mui/styles';
import Stack from '@mui/material/Stack';

const useStyles = makeStyles({
  infoTitle: {
    fontWeight: 500,
    textTransform: 'uppercase',
  },
  bold: {
    fontWeight: 500,
  },
});

const CalcService = () => {
  const { selectedServices, discount } = useContext(WizardContext);

  const classes = useStyles();

  const calcValue = (type) => {
    let sum = 0;
    let disc = 0;
    let total = 0;

    sum = selectedServices
      .filter((el) => el.selected)
      .map((item) => parseInt(item.price))
      .reduce((prev, next) => prev + next, 0);

    disc = (sum * discount) / 100;

    total = sum - disc;

    if (type === 'calcSum') {
      return sum.toLocaleString();
    } else if (type === 'calcDiscount') {
      return disc.toLocaleString();
    } else if (type === 'calcTotal') {
      return total.toLocaleString();
    } else {
      return '0';
    }
  };

  return (
    <Fragment>
      <Stack
        direction="row"
        spacing={2}
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="p" className={classes.infoTitle}>
          Osnovica:
        </Typography>
        <Typography variant="subtitle1" sx={{ fontWeight: 700 }} align="right">
          {calcValue('calcSum')} kn
        </Typography>
      </Stack>
      <Stack
        direction="row"
        spacing={2}
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="p" className={classes.infoTitle}>
          Popust ({discount}%):
        </Typography>
        <Typography variant="subtitle1" sx={{ fontWeight: 700 }} align="right">
          - {calcValue('calcDiscount')} kn
        </Typography>
      </Stack>
      <Stack
        direction="row"
        spacing={2}
        alignItems="center"
        justifyContent="space-between"
      >
        <Typography variant="p" className={classes.infoTitle}>
          Ukupno:
        </Typography>
        <Typography variant="subtitle1" sx={{ fontWeight: 700 }} align="right">
          {calcValue('calcTotal')} kn
        </Typography>
      </Stack>
    </Fragment>
  );
};

export default CalcService;
