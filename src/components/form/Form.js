import React, { Fragment, useContext } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import FormCarDetails from './FormCarDetails';
import FormServiceDetails from './FormServiceDetails';
import FormCustomer from './FormCustomer';
import Confirm from './Confirm';
import Success from './Success';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

const UserForm = () => {
  const { step } = useContext(WizardContext);

  // Show Form
  const showForm = () => {
    switch (step) {
      case 1:
        return <FormCarDetails title="Odaberite proizvođača vašeg vozila" />;
      case 2:
        return (
          <FormServiceDetails title="Odaberite jednu ili više usluga za koje ste" />
        );
      case 3:
        return <FormCustomer title="Vaši kontakt podaci" />;
      case 4:
        return (
          <Confirm
            title="Pregled i potvrda vašeg odabira"
            infoText="Molimo vas da još jednom pregledate i potvrdite unesene podatke. Ukoliko želite promijeniti neki od podataka možete pritisnuti gumb za uređivanje pored svake od kategorija. Kada ste provjeriti i potvrdili ispravnost svojih podataka pritisnite gumb pošalji na dnu, za slanje upita za servis."
          />
        );
      case 5:
        return <Success />;
      default:
        return;
    }
  };

  return (
    <Fragment>
      <Typography component="h1" variant="h4" align="center" gutterBottom>
        Konfigurator servisa
      </Typography>
      <Box mt={4}>{showForm()}</Box>
    </Fragment>
  );
};

export default UserForm;
