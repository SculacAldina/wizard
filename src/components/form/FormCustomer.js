import React, { Fragment, useContext } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const FormCustomer = (props) => {
  const { contactInfo, step, nextStep, prevStep, textInput } =
    useContext(WizardContext);

  const { title } = props;

  const continueForm = (e) => {
    e.preventDefault();
    nextStep();
  };

  const backForm = (e) => {
    e.preventDefault();
    prevStep();
  };

  return (
    <Fragment>
      <Typography variant="h6" gutterBottom>
        {step}. {title}
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="fullName"
            name="fullName"
            label="Ime i prezime"
            fullWidth
            autoComplete="name"
            variant="standard"
            onChange={textInput('fullName')}
            defaultValue={contactInfo.fullName}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="email"
            name="email"
            label="Email adresa"
            fullWidth
            autoComplete="email"
            variant="standard"
            onChange={textInput('email')}
            defaultValue={contactInfo.email}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="phone"
            name="phone"
            label="Broj telefona"
            fullWidth
            autoComplete="tel"
            variant="standard"
            onChange={textInput('phone')}
            defaultValue={contactInfo.phone}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            id="message"
            label="Napomena"
            multiline
            maxRows={4}
            onChange={textInput('message')}
            defaultValue={contactInfo.message}
            fullWidth
            variant="standard"
          />
        </Grid>
      </Grid>
      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button variant="outlined" onClick={backForm} sx={{ mt: 3, ml: 1 }}>
          Nazad
        </Button>

        <Button
          variant="contained"
          onClick={continueForm}
          sx={{ mt: 3, ml: 1 }}
          disabled={
            contactInfo.fullName === ''
              ? true
              : contactInfo.email === ''
              ? true
              : contactInfo.phone === ''
              ? true
              : false
          }
        >
          Dalje
        </Button>
      </Box>
    </Fragment>
  );
};

export default FormCustomer;
