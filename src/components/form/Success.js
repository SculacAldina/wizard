import React, { Fragment } from 'react';
import Typography from '@mui/material/Typography';

const Success = () => {
  return (
    <Fragment>
      <Typography variant="h6" gutterBottom align="center">
        Vaša prijava je uspješno poslana
      </Typography>
      <Typography variant="subtitle1" gutterBottom align="center">
        Vaša prijava je uspješno poslana i zaprimljena. Kontaktirat ćemo vas u
        najkraćem mogućem roku. Hvala vam.
      </Typography>
    </Fragment>
  );
};

export default Success;
