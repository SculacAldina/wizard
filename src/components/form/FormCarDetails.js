import React, { Fragment, useContext } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import Typography from '@mui/material/Typography';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

const FormCarDetails = (props) => {
  const { radioInput, step, nextStep, carBrand, car_brand } =
    useContext(WizardContext);

  const { title } = props;
  const continueForm = (e) => {
    e.preventDefault();
    nextStep();
  };

  return (
    <Fragment>
      <Typography variant="h6" gutterBottom>
        {step}. {title}
      </Typography>
      <FormControl component="fieldset">
        <RadioGroup
          row
          aria-label="car_brand"
          name="car_brand"
          onChange={(e) => radioInput(e.target.value)}
          value={carBrand}
        >
          {car_brand.map((car) => {
            return (
              <FormControlLabel
                value={car}
                control={<Radio />}
                label={car}
                key={car}
              />
            );
          })}
        </RadioGroup>
      </FormControl>
      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          variant="contained"
          onClick={continueForm}
          sx={{ mt: 3, ml: 1 }}
          disabled={carBrand === '' ? true : false}
        >
          Dalje
        </Button>
      </Box>
    </Fragment>
  );
};

export default FormCarDetails;
