import React, { Fragment, useContext } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import CalcService from './CalcService';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { makeStyles } from '@mui/styles';
import Stack from '@mui/material/Stack';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import Divider from '@mui/material/Divider';

const useStyles = makeStyles({
  infoTitle: {
    fontWeight: 600,
    textTransform: 'uppercase',
  },
  bold: {
    fontWeight: 500,
  },
});

const Confirm = (props) => {
  const {
    carBrand,
    selectedServices,
    contactInfo,
    step,
    prevStep,
    jumpToStep,
    sendForm,
  } = useContext(WizardContext);

  const { title, infoText } = props;
  const continueForm = (e) => {
    e.preventDefault();
    sendForm();
  };

  const backForm = (e) => {
    e.preventDefault();
    prevStep();
  };

  const classes = useStyles();

  return (
    <Fragment>
      <Typography variant="h6" gutterBottom>
        {step}. {title}
      </Typography>
      <Typography variant="p" gutterBottom>
        {infoText}
      </Typography>

      <Grid container spacing={3} mt={4} mb={4}>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Stack direction="row" spacing={2} alignItems="center">
              <Typography variant="p" className={classes.infoTitle}>
                Model vozila
              </Typography>
              <IconButton
                color="primary"
                component="span"
                size="small"
                onClick={() => jumpToStep(1)}
              >
                <EditIcon fontSize="inherit" />
              </IconButton>
            </Stack>
            <Typography variant="p" gutterBottom>
              {carBrand}
            </Typography>
          </Stack>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Stack direction="row" spacing={2} alignItems="center">
              <Typography variant="p" className={classes.infoTitle}>
                Odabrane usluge
              </Typography>
              <IconButton
                color="primary"
                component="span"
                size="small"
                onClick={() => jumpToStep(2)}
              >
                <EditIcon fontSize="inherit" />
              </IconButton>
            </Stack>

            <List disablePadding>
              {selectedServices.map((el) => {
                let list = '';
                if (el.selected === true) {
                  list = (
                    <ListItem key={el.id} sx={{ py: 0, px: 0 }}>
                      <ListItemText primary={el.type} />
                      <Typography variant="body2">{el.price} kn</Typography>
                    </ListItem>
                  );
                }
                return list;
              })}
              <Divider />
              <CalcService />
            </List>
          </Stack>
        </Grid>
      </Grid>
      <Divider />

      <Grid container spacing={3} mt={2}>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Stack direction="row" spacing={2} alignItems="center">
              <Typography variant="p" className={classes.infoTitle}>
                Kontakt podaci
              </Typography>
              <IconButton
                color="primary"
                component="span"
                size="small"
                onClick={() => jumpToStep(3)}
              >
                <EditIcon fontSize="inherit" />
              </IconButton>
            </Stack>
          </Stack>
        </Grid>
      </Grid>
      <Grid container mt={2}>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Typography variant="p" gutterBottom>
              Ime i prezime: {contactInfo.fullName}
            </Typography>
          </Stack>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Typography variant="p" gutterBottom>
              Email adresa: {contactInfo.email}
            </Typography>
          </Stack>
        </Grid>
      </Grid>
      <Grid container mt={2}>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Typography variant="p" gutterBottom>
              Broj telefona: {contactInfo.phone}
            </Typography>
          </Stack>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack spacing={2}>
            <Typography variant="p" gutterBottom>
              Napomena: {contactInfo.message ? contactInfo.message : '/'}
            </Typography>
          </Stack>
        </Grid>
      </Grid>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button variant="outlined" onClick={backForm} sx={{ mt: 3, ml: 1 }}>
          Nazad
        </Button>

        <Button
          variant="contained"
          onClick={continueForm}
          sx={{ mt: 3, ml: 1 }}
        >
          Pošalji
        </Button>
      </Box>
    </Fragment>
  );
};

export default Confirm;
