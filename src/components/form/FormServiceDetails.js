import React, { Fragment, useState, useContext } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import CalcService from './CalcService';
import Typography from '@mui/material/Typography';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';

const FormServiceDetails = (props) => {
  const {
    checkboxInput,
    step,
    nextStep,
    prevStep,
    selectedServices,
    getDiscount,
    discountCode,
    discount,
  } = useContext(WizardContext);

  const [text, setText] = useState('');
  const [openInputField, setOpenInputField] = useState(false);
  const [showError, setShowError] = useState(false);
  const [showMessage, setShowMessage] = useState(false);

  const { title } = props;

  const continueForm = (e) => {
    e.preventDefault();
    nextStep();
  };

  const backForm = (e) => {
    e.preventDefault();
    prevStep();
  };

  // Handle Checkbox
  const handleCheckbox = (e) => {
    const value = e.target.value;
    let checked = e.target.checked;
    checkboxInput(value, checked);
  };

  const isSelected = selectedServices.filter((item) => {
    if (item.selected === true) {
      return true;
    }
  });

  // Check Discount Code
  const checkDiscount = () => {
    if (discountCode === text) {
      setShowError(false);
      setShowMessage(true);
      getDiscount();
    } else {
      setShowError(true);
      setShowMessage(false);
    }
  };

  const showDiscountField = () => {
    if (!showMessage && discount === 0) {
      return (
        <Fragment>
          <TextField
            error={showError}
            id="discountCode"
            name="discountCode"
            label="Unesite kod kupona"
            variant="standard"
            onChange={(e) => setText(e.target.value)}
            helperText={showError ? 'Neispravan kod.' : ''}
          />
          <Button variant="text" onClick={checkDiscount}>
            Provjeri
          </Button>
        </Fragment>
      );
    } else {
      return (
        <Typography component="div" variant="caption">
          <Box sx={{ color: 'success.main' }}>
            Hvala vam, unijeli ste ispravan kod kupona
          </Box>
        </Typography>
      );
    }
  };

  return (
    <Fragment>
      <Typography variant="h6" gutterBottom>
        {step}. {title}
      </Typography>
      <FormGroup row>
        {selectedServices.map((service) => {
          let labelText = `${service.type} (${service.price} kn)`;
          return (
            <FormControlLabel
              mr={3}
              key={service.id}
              label={labelText}
              control={
                <Checkbox
                  checked={service.selected}
                  name={service.type}
                  onChange={handleCheckbox}
                  value={service.id}
                />
              }
            />
          );
        })}
      </FormGroup>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }} mb={2}>
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="center"
        >
          {!openInputField && discount === 0 ? (
            <Button
              variant="text"
              onClick={() => setOpenInputField(true)}
              sx={{ mt: 1, ml: 1 }}
            >
              Imam kupon
            </Button>
          ) : (
            showDiscountField()
          )}
        </Stack>
      </Box>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }} mb={2}>
        <Stack>
          <CalcService />
        </Stack>
      </Box>

      <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button variant="outlined" onClick={backForm} sx={{ mt: 3, ml: 1 }}>
          Nazad
        </Button>

        <Button
          variant="contained"
          onClick={continueForm}
          sx={{ mt: 3, ml: 1 }}
          disabled={isSelected.length === 0 ? true : false}
        >
          Dalje
        </Button>
      </Box>
    </Fragment>
  );
};

export default FormServiceDetails;
