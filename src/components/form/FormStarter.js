import React, { Fragment, useContext, useState } from 'react';
import WizardContext from '../../context/wizard/wizardContext';
import Form from './Form';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Modal from '@mui/material/Modal';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '60vw',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 4,
};

const FormStarter = () => {
  const { clearState, step } = useContext(WizardContext);

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    clearState();
  };

  return (
    <Fragment>
      <Box
        sx={{
          bgcolor: 'background.paper',
          pt: 8,
          pb: 6,
        }}
      >
        <Container maxWidth="sm">
          <Typography
            component="h1"
            variant="subtitle1"
            align="center"
            color="text.primary"
            gutterBottom
          >
            Pritisnite gumb niže kako biste pokrenuli
          </Typography>
          <Stack sx={{ pt: 4 }} direction="row" justifyContent="center">
            <div>
              <Button variant="contained" onClick={handleOpen}>
                Pokreni konfigurator
              </Button>
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <Box sx={style}>
                  <Form />
                </Box>
              </Modal>
            </div>
          </Stack>
        </Container>
      </Box>
    </Fragment>
  );
};

export default FormStarter;
