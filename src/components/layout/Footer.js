import React from 'react';
import Typography from '@mui/material/Typography';

const Footer = () => {
  return (
    <Typography variant="body2" color="text.secondary" textAlign="center">
      {'Copyright © Konfigurator servisa '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
};

export default Footer;
