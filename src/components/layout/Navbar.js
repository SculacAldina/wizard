import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import BuildCircleIcon from '@mui/icons-material/BuildCircle';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { makeStyles } from '@mui/styles';
import logo from '../../img/tokic.svg';

const theme = createTheme();

const useStyles = makeStyles({
  logo: {
    maxWidth: '100px',
    marginRight: '1rem',
  },
});

const Navbar = (props) => {
  const { title, subtitle } = props;

  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <Stack direction="row" alignItems="center">
            {/* <BuildCircleIcon sx={{ mr: 2 }} /> */}
            <img src={logo} alt="logo" className={classes.logo} />
            <Stack direction="column" justifyContent="center">
              <Typography variant="subtitle11" color="inherit" noWrap>
                {title}
              </Typography>
              <Typography variant="subtitle2" color="inherit" noWrap>
                {subtitle}
              </Typography>
            </Stack>
          </Stack>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
};

Navbar.defaultProps = {
  title: 'Konfigurator servisa',
  subtitle: 'Izračunajte trošak servisa',
};

Navbar.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
};

export default Navbar;
