import React from 'react';
import WizardState from './context/wizard/WizardState';
import FormStarter from './components/form/FormStarter';
import Footer from './components/layout/Footer';
import Navbar from './components/layout/Navbar';

import CssBaseline from '@mui/material/CssBaseline';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';

export default function App() {
  return (
    <WizardState>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: '100vh',
        }}
      >
        <CssBaseline />
        <Navbar />
        <Container component="main" sx={{ mt: 8, mb: 2 }} maxWidth="sm">
          <FormStarter />
        </Container>
        <Box
          component="footer"
          sx={{
            py: 3,
            px: 2,
            mt: 'auto',
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[200]
                : theme.palette.grey[800],
          }}
        >
          <Container maxWidth="sm">
            <Footer />
          </Container>
        </Box>
      </Box>
    </WizardState>
  );
}
