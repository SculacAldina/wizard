import { createContext } from 'react';

const wizardContext = createContext();

export default wizardContext;
