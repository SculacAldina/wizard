import {
  GET_RADIO_INPUT,
  GET_CHECKBOX_INPUT,
  GET_TEXT_INPUT,
  NEXT_STEP,
  PREV_STEP,
  JUMP_STEP,
  CLEAR_STATE,
  GET_DISCOUNT,
} from '../types';

export default (state, action) => {
  switch (action.type) {
    case GET_RADIO_INPUT:
      return {
        ...state,
        carBrand: action.payload,
      };
    case GET_CHECKBOX_INPUT:
      return {
        ...state,
        selectedServices: state.selectedServices.map((el) =>
          el.id === parseInt(action.payload.value)
            ? { ...el, selected: action.payload.checked }
            : el
        ),
      };
    case GET_TEXT_INPUT:
      return {
        ...state,
        contactInfo: {
          ...state.contactInfo,
          [action.payload.input]: action.payload.value,
        },
      };
    case NEXT_STEP:
      return {
        ...state,
        step: state.step + action.payload,
      };
    case PREV_STEP:
      return {
        ...state,
        step: state.step - action.payload,
      };
    case JUMP_STEP:
      return {
        ...state,
        step: action.payload,
      };
    case CLEAR_STATE:
      return {
        ...action.payload,
      };
    case GET_DISCOUNT:
      return {
        ...state,
        discount: action.payload,
      };
    default:
      return state;
  }
};
