import React, { useReducer } from 'react';
import WizardContext from './wizardContext';
import WizardReducer from './wizardReducer';

import {
  GET_RADIO_INPUT,
  GET_CHECKBOX_INPUT,
  GET_TEXT_INPUT,
  NEXT_STEP,
  PREV_STEP,
  JUMP_STEP,
  CLEAR_STATE,
  GET_DISCOUNT,
} from '../types';

const service = [
  {
    id: 1,
    type: 'Zamjena ulja i filtera',
    price: '500',
    selected: false,
  },
  {
    id: 2,
    type: 'Promjena pakni',
    price: '450',
    selected: false,
  },
  {
    id: 3,
    type: 'Promjena guma',
    price: '100',
    selected: false,
  },
  {
    id: 4,
    type: 'Servis klima uređaja',
    price: '299',
    selected: false,
  },
  {
    id: 5,
    type: 'Balansiranje guma',
    price: '50',
    selected: false,
  },
  {
    id: 6,
    type: 'Zamjena ulja u kočnicama',
    price: '299',
    selected: false,
  },
];

const car_brand = [
  'Peugeot',
  'Volkswagen',
  'Citroen',
  'Audi',
  'BMW',
  'Seat',
  'Alfa Romeo',
  'Kia',
  'Hyundai',
  'Honda',
  'Toyota',
];

const WizardState = (props) => {
  const initialState = {
    step: 1,
    discountCode: 'Tokić123',
    carBrand: '',
    selectedServices: service,
    car_brand: car_brand,
    discount: 0,
    contactInfo: {
      fullName: '',
      phone: '',
      email: '',
      message: '',
    },
  };

  const [state, dispatch] = useReducer(WizardReducer, initialState);
  console.log(state);

  // Next Step
  const nextStep = () => {
    dispatch({
      type: NEXT_STEP,
      payload: 1,
    });
  };

  // Next Step
  const prevStep = () => {
    dispatch({
      type: PREV_STEP,
      payload: 1,
    });
  };

  // Jump to Step
  const jumpToStep = (jumpTo) => {
    dispatch({
      type: JUMP_STEP,
      payload: jumpTo,
    });
  };

  // Handle Radio Input - Car Input
  const radioInput = (value) => {
    dispatch({
      type: GET_RADIO_INPUT,
      payload: value,
    });
  };

  // Handle Checkbox Input - Service Input
  const checkboxInput = (value, checked) => {
    dispatch({
      type: GET_CHECKBOX_INPUT,
      payload: { value, checked },
    });
  };

  // Handle Text Input
  const textInput = (input) => (e) => {
    const value = e.target.value;
    dispatch({
      type: GET_TEXT_INPUT,
      payload: { value, input },
    });
  };

  // Get Discount
  const getDiscount = () => {
    dispatch({
      type: GET_DISCOUNT,
      payload: 30,
    });
  };

  // Send Form
  const sendForm = () => {
    console.log('Upit je poslan.');
    dispatch({
      type: NEXT_STEP,
      payload: 1,
    });
  };

  // Clear State
  const clearState = () => {
    dispatch({
      type: CLEAR_STATE,
      payload: initialState,
    });
  };

  return (
    <WizardContext.Provider
      value={{
        step: state.step,
        discountCode: state.discountCode,
        discount: state.discount,
        carBrand: state.carBrand,
        selectedServices: state.selectedServices,
        dicount: state.dicount,
        contactInfo: state.contactInfo,
        car_brand: state.car_brand,
        radioInput,
        checkboxInput,
        textInput,
        getDiscount,
        nextStep,
        prevStep,
        jumpToStep,
        sendForm,
        clearState,
      }}
    >
      {props.children}
    </WizardContext.Provider>
  );
};

export default WizardState;
