export const GET_RADIO_INPUT = 'GET_RADIO_INPUT';
export const GET_CHECKBOX_INPUT = 'GET_CHECKBOX_INPUT';
export const GET_TEXT_INPUT = 'GET_TEXT_INPUT';
export const NEXT_STEP = 'NEXT_STEP';
export const PREV_STEP = 'PREV_STEP';
export const JUMP_STEP = 'JUMP_STEP';
export const CLEAR_STATE = 'CLEAR_STATE';
export const GET_DISCOUNT = 'GET_DISCOUNT';
